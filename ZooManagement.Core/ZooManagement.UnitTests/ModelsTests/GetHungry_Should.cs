﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ZooManagement.Models;

namespace ZooManagement.UnitTests.ModelsTests
{
    [TestClass]
    public class GetHungry_Should
    {
        [TestMethod]
        public void DecreaseHealth()
        {
            //Arrange
            var bear = new Bear(1, "Teddy");

            //Act
            var result = bear.GetHungry(25);

            //Assert
            Assert.IsTrue(bear.Health == 75);
            Assert.IsTrue(result.Contains(bear.Name));
        }

        [TestMethod]
        public void ReturnMessage_When_AnimalIsDead()
        {
            //Arrange
            var bear = new Bear(1, "Teddy");
            bear.GetHungry(35);

            //Act
            var result = bear.GetHungry(15);

            //Assert
            Assert.IsTrue(bear.Health == 50);
            Assert.IsTrue(bear.IsDead);
            Assert.IsTrue(result.Contains("dead"));
        }

        [TestMethod]
        public void DecreaseHealthTo0()
        {
            //Arrange
            var bear = new Bear(1, "Teddy");
            bear.GetHungry(35);
            bear.GetHungry(35);

            //Act
            var result = bear.GetHungry(35);

            //Assert
            Assert.IsTrue(bear.Health == 0);
            Assert.IsTrue(bear.IsDead);
            Assert.IsTrue(result.Contains("dead"));
        }

        [TestMethod]
        public void KillGiraffe_When_CantMoveNeck_And_HealthDecreased()
        {
            //Arrange
            var giraffe = new Giraffe(1, "Long Neck");
            giraffe.GetHungry(25);
            giraffe.GetHungry(15);

            //Act
            var result = giraffe.GetHungry(15);

            //Assert
            Assert.IsTrue(giraffe.Health == 45);
            Assert.IsFalse(giraffe.CanMoveNeck);
            Assert.IsTrue(giraffe.IsDead);
            Assert.IsTrue(result.Contains("dead"));
        }

        [TestMethod]
        public void KillMonkey_When_HealthUnder40()
        {
            //Arrange
            var monkey = new Giraffe(1, "Long Neck");
            monkey.GetHungry(35);
            monkey.GetHungry(15);

            //Act
            var result = monkey.GetHungry(15);

            //Assert
            Assert.IsTrue(monkey.Health == 35);
            Assert.IsTrue(monkey.IsDead);
            Assert.IsTrue(result.Contains("dead"));
        }
    }
}
