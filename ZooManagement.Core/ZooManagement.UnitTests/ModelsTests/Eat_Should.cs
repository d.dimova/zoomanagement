﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using ZooManagement.Models;

namespace ZooManagement.UnitTests.ModelsTests
{
    [TestClass]
    public class Eat_Should
    {
        [TestMethod]
        public void IncreaseHealth()
        {
            //Arrange
            var bear = new Bear(1, "Teddy");
            bear.GetHungry(25);

            //Act
            var result = bear.Eat(15);

            //Assert
            Assert.IsTrue(bear.Health == 90);
            Assert.IsTrue(result.Contains(bear.Name));
        }

        [TestMethod]
        public void IncreaseHealthTo100_When_TooMuchFoodGiven()
        {
            //Arrange
            var bear = new Bear(1, "Teddy");
            bear.GetHungry(15);

            //Act
            var result = bear.Eat(25);

            //Assert
            Assert.IsTrue(bear.Health == 100);
            Assert.IsTrue(result.Contains(bear.Name));
        }
    }
}
