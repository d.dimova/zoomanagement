﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ZooManagement.Models;
using ZooManagement.Models.Abstracts;

namespace ZooManagement.UnitTests.ModelsTests
{ 
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void ThrowException_When_NameIsNull()
        {
            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Bear(1, null));
        }

        [TestMethod]
        public void ThrowException_When_NameIsEmpty()
        {
            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Bear(1, string.Empty));
        }

        [TestMethod]
        public void ThrowException_When_NameIsWhiteSpace()
        {
            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Bear(1, " "));
        }

        [TestMethod]
        public void InitializeProperties_Bear()
        {
            //Arrange & Act
            var bear = new Bear(1, "Teddy");

            //Assert
            Assert.IsInstanceOfType(bear, typeof(Animal));
            Assert.IsInstanceOfType(bear, typeof(Bear));
            Assert.IsTrue(bear.ID == 1);
            Assert.IsTrue(bear.Name == "Teddy");
            Assert.IsTrue(bear.Health == 100);
            Assert.IsFalse(bear.IsDead);
        }

        [TestMethod]
        public void InitializeProperties_Giraffe()
        {
            //Arrange & Act
            var giraffe = new Giraffe(1, "Maury");

            //Assert
            Assert.IsInstanceOfType(giraffe, typeof(Animal));
            Assert.IsInstanceOfType(giraffe, typeof(Giraffe));
            Assert.IsTrue(giraffe.CanMoveNeck);
        }
    }
}
