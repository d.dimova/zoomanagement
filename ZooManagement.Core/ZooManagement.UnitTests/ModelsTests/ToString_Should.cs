﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using ZooManagement.Models;

namespace ZooManagement.UnitTests.ModelsTests
{
    [TestClass]
    public class ToString_Should
    {
        [TestMethod]
        public void ReturnInfo_Bear()
        {
            //Arrange
            var bear = new Bear(1, "Teddy");

            //Act
            var result = bear.ToString();

            //Assert
            Assert.IsTrue(result.Contains(bear.Name));
            Assert.IsTrue(result.Contains(bear.Health.ToString()));
        }

        [TestMethod]
        public void ReturnInfo_Giraffe()
        {
            //Arrange
            var giraffe = new Giraffe(1, "Spotty");

            //Act
            var result = giraffe.ToString();

            //Assert
            Assert.IsTrue(result.Contains(giraffe.Name));
            Assert.IsTrue(result.Contains(giraffe.Health.ToString()));
            Assert.IsTrue(result.Contains(giraffe.CanMoveNeck.ToString()));
        }

        [TestMethod]
        public void ReturnInfo_Monkey()
        {
            //Arrange
            var monkey = new Monkey(1, "Bunzo");

            //Act
            var result = monkey.ToString();

            //Assert
            Assert.IsTrue(result.Contains(monkey.Name));
            Assert.IsTrue(result.Contains(monkey.Health.ToString()));
        }
    }
}
