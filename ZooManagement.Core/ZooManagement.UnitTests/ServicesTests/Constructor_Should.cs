﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using ZooManagement.Contracts.Data;
using ZooManagement.Services.Commands;
using ZooManagement.Services.Contracts;

namespace ZooManagement.UnitTests.ServicesTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void CreateCommand()
        {
            //Arrange
            var mockDB = new Mock<IDatabase>();
            var mockFactory = new Mock<IFactory>();

            //Act
            var command = new MenuCommand(new List<string>(), mockDB.Object, mockFactory.Object);

            //Assert
            Assert.IsInstanceOfType(command, typeof(MenuCommand));
        }
    }
}
