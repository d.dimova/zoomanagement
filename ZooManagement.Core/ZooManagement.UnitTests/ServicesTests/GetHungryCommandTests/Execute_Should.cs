﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Models.Abstracts;
using ZooManagement.Services.Commands;
using ZooManagement.Services.Contracts;

namespace ZooManagement.UnitTests.ServicesTests.GetHungryCommandTests
{
    [TestClass]
    public class Execute_Should
    {
        [TestMethod]
        public void ThrowException_When_ParametersCountIsNot0_GetHungryCommand()
        {
            //Arrange
            var mockDB = new Mock<IDatabase>();
            var mockFactory = new Mock<IFactory>();

            var command = new GetHungryCommand(new List<string>() { "" }, mockDB.Object, mockFactory.Object);

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => command.Execute(), Strings.CommandNotVallid);
        }

        [TestMethod]
        public void ReduceHealthOfAllAnimals()
        {
            //Arrange
            var animals = TestSetup.GetAnimals().ToList();

            var mockDB = new Mock<IDatabase>();
            mockDB.SetupGet<List<Animal>>(m => m.Animals).Returns(animals);
            mockDB.Setup(m => m.AddAnimal(It.IsAny<Animal>()));

            var mockFactory = new Mock<IFactory>();

            var command = new GetHungryCommand(new List<string>(), mockDB.Object, mockFactory.Object);

            //Act
            var result = command.Execute();

            //Assert
            Assert.IsFalse(result.Contains("100"));
            foreach (var item in animals.Select(a => a.Name))
            {
                Assert.IsTrue(result.Contains(item));
            }
        }
    }
}
