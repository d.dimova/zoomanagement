﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Models;
using ZooManagement.Models.Abstracts;
using ZooManagement.Services.Commands;
using ZooManagement.Services.Contracts;

namespace ZooManagement.UnitTests.ServicesTests.AddAnimalCommandTests
{
    [TestClass]
    public class Execute_Should
    {
        [TestMethod]
        public void ThrowException_When_ParametersCountIsNot2()
        {
            //Arrange
            var mockDB = new Mock<IDatabase>();
            var mockFactory = new Mock<IFactory>();

            var command = new AddAnimalCommand(new List<string>(), mockDB.Object, mockFactory.Object);

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => command.Execute(), Strings.CommandNotVallid);
        }

        [TestMethod]
        public void ThrowException_When_InvalidAnimalPassed()
        {
            //Arrange
            var mockDB = new Mock<IDatabase>();
            mockDB.SetupGet<List<Animal>>(m => m.Animals).Returns(new List<Animal>());
            var mockFactory = new Mock<IFactory>();

            var command = new AddAnimalCommand(new List<string>() { "snake", "Jumbo" }, mockDB.Object, mockFactory.Object);

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => command.Execute(), Strings.InvalidAnimalType);
        }

        [TestMethod]
        public void AddNewAnimal()
        {
            //Arrange
            var bear = new Bear(1, "Teddy");

            var mockDB = new Mock<IDatabase>();
            mockDB.SetupGet<List<Animal>>(m => m.Animals).Returns(new List<Animal>());
            mockDB.Setup(m => m.AddAnimal(It.IsAny<Animal>()));

            var mockFactory = new Mock<IFactory>();
            mockFactory.Setup(m => m.CreateBear(1, "Teddy")).Returns(bear);

            var command = new AddAnimalCommand(new List<string>() { "bear", "Teddy" }, mockDB.Object, mockFactory.Object);

            //Act
            var result = command.Execute();
            
            //Assert
            Assert.AreEqual(String.Format(Strings.AnimalIsAdded, bear.Name), result);
        }
    }
}
