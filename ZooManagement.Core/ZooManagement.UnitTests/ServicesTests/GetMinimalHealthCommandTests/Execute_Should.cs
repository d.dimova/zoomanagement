﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Models.Abstracts;
using ZooManagement.Services.Commands;
using ZooManagement.Services.Contracts;

namespace ZooManagement.UnitTests.ServicesTests.GetMinimalHealthCommandTests
{
    [TestClass]
    public class Execute_Should
    {
        [TestMethod]
        public void ThrowException_When_ParametersCountIsNot1_GetMinimalHealthCommand()
        {
            //Arrange
            var mockDB = new Mock<IDatabase>();
            var mockFactory = new Mock<IFactory>();

            var command = new GetMinimalHealthCommand(new List<string>(), mockDB.Object, mockFactory.Object);

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => command.Execute(), Strings.CommandNotVallid);
        }

        [TestMethod]
        public void ThrowException_When_InvalidAnimalPassed()
        {
            //Arrange
            var mockDB = new Mock<IDatabase>();
            mockDB.SetupGet<List<Animal>>(m => m.Animals).Returns(new List<Animal>());
            var mockFactory = new Mock<IFactory>();

            var command = new GetMinimalHealthCommand(new List<string>() { "snake" }, mockDB.Object, mockFactory.Object);

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => command.Execute(), Strings.InvalidAnimalType);
        }

        [TestMethod]
        public void ReturnNoAnimal()
        {
            //Arrange
            var animals = TestSetup.GetAnimals().ToList();

            foreach (var item in animals)
            {
                item.GetHungry(35);
                item.GetHungry(25);
            }

            var mockDB = new Mock<IDatabase>();
            mockDB.SetupGet<List<Animal>>(m => m.Animals).Returns(animals);
            mockDB.Setup(m => m.AddAnimal(It.IsAny<Animal>()));

            var mockFactory = new Mock<IFactory>();

            var command = new GetMinimalHealthCommand(new List<string>() { "bear" }, mockDB.Object, mockFactory.Object);

            //Act
            var result = command.Execute();

            //Assert
            Assert.AreEqual(String.Format(Strings.AllAnymalsAreDead, "bear"), result);
        }


        [TestMethod]
        public void ReturnAnimal_Bear()
        {
            //Arrange
            var animals = TestSetup.GetAnimals().ToList();

            foreach (var item in animals)
            {
                item.GetHungry(20);
            }

            animals[0].GetHungry(15);

            var mockDB = new Mock<IDatabase>();
            mockDB.SetupGet<List<Animal>>(m => m.Animals).Returns(animals);
            mockDB.Setup(m => m.AddAnimal(It.IsAny<Animal>()));

            var mockFactory = new Mock<IFactory>();

            var command = new GetMinimalHealthCommand(new List<string>() { "bear" }, mockDB.Object, mockFactory.Object);

            //Act
            var result = command.Execute();

            //Assert

            Assert.IsTrue(result.Contains(animals[0].Name));
        }
    }
}
