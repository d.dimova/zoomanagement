﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Models.Abstracts;
using ZooManagement.Services.Commands;
using ZooManagement.Services.Contracts;

namespace ZooManagement.UnitTests.ServicesTests.ViewAllAnimalsCommandTests
{
    [TestClass]
    public class Execute_Should
    {
        [TestMethod]
        public void ThrowException_When_ParametersCountIsNot0_ViewAllAnimalsCommand()
        {
            //Arrange
            var mockDB = new Mock<IDatabase>();
            var mockFactory = new Mock<IFactory>();

            var command = new ViewAllAnimalsCommand(new List<string>() { "" }, mockDB.Object, mockFactory.Object);

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => command.Execute(), Strings.CommandNotVallid);
        }

        [TestMethod]
        public void ReturnNoAnimals_ViewAllAnimalsCommand()
        {
            //Arrange
            var animals = TestSetup.GetAnimals().ToList();

            foreach(var item in animals)
            {
                item.GetHungry(35);
                item.GetHungry(35);
                item.GetHungry(15);
            }

            var mockDB = new Mock<IDatabase>();
            mockDB.SetupGet<List<Animal>>(m => m.Animals).Returns(animals);
            mockDB.Setup(m => m.AddAnimal(It.IsAny<Animal>()));

            var mockFactory = new Mock<IFactory>();

            var command = new ViewAllAnimalsCommand(new List<string>(), mockDB.Object, mockFactory.Object);

            //Act
            var result = command.Execute();

            //Assert
            Assert.AreEqual(String.Format(Strings.AllAnymalsAreDead, "animal"), result);
        }

        [TestMethod]
        public void ReturnAllAliveAnimals()
        {
            //Arrange
            var animals = TestSetup.GetAnimals().ToList();

            animals[0].GetHungry(35);
            animals[0].GetHungry(35);

            var mockDB = new Mock<IDatabase>();
            mockDB.SetupGet<List<Animal>>(m => m.Animals).Returns(animals);
            mockDB.Setup(m => m.AddAnimal(It.IsAny<Animal>()));

            var mockFactory = new Mock<IFactory>();

            var command = new ViewAllAnimalsCommand(new List<string>(), mockDB.Object, mockFactory.Object);

            //Act
            var result = command.Execute();

            //Assert
            Assert.IsFalse(result.Contains(animals[0].Name));
        }
    }
}
