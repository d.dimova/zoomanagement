﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using ZooManagement.Models;
using ZooManagement.Services;

namespace ZooManagement.UnitTests.FactoryTests
{
    [TestClass]
    public class CreateBear_Should
    {
        [TestMethod]
        public void CreateRecord_Bear()
        {
            //Arrange
            var factory = new Factory();

            //Act
            var bear = factory.CreateBear(1, "Sloppy");

            //Assert
            Assert.IsInstanceOfType(bear, typeof(Bear));
        }
    }
}
