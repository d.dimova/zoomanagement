﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using ZooManagement.Models;
using ZooManagement.Services;

namespace ZooManagement.UnitTests.FactoryTests
{
    [TestClass]
    public class CreateMonkey_Should
    {
        [TestMethod]
        public void CreateRecord_Monkey()
        {
            //Arrange
            var factory = new Factory();

            //Act
            var monkey = factory.CreateMonkey(1, "Gumbo");

            //Assert
            Assert.IsInstanceOfType(monkey, typeof(Monkey));
        }
    }
}
