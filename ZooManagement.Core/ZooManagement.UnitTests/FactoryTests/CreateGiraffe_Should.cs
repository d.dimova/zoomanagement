﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using ZooManagement.Models;
using ZooManagement.Services;

namespace ZooManagement.UnitTests.FactoryTests
{
    [TestClass]
    public class CreateGiraffe_Should
    {
        [TestMethod]
        public void CreateRecord_Giraffe()
        {
            //Arrange
            var factory = new Factory();

            //Act
            var giraffe = factory.CreateGiraffe(1, "Long Neck");

            //Assert
            Assert.IsInstanceOfType(giraffe, typeof(Giraffe));
        }
    }
}
