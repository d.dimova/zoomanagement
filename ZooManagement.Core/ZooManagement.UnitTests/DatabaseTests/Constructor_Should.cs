﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using ZooManagement.Data;
using ZooManagement.Models.Abstracts;

namespace ZooManagement.UnitTests.DatabaseTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void SeedData()
        {
            //Arrange & Act
            var database = new Database();

            //Assert
            Assert.IsInstanceOfType(database.Animals, typeof(List<Animal>));
            Assert.AreEqual(30, database.Animals.Count());
        }
    }
}
