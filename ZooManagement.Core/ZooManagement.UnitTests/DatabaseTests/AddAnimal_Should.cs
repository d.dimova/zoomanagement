﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooManagement.Data;

namespace ZooManagement.UnitTests.DatabaseTests
{
    [TestClass]
    public class AddAnimal_Should
    {
        [TestMethod]
        public void AddNewRecord()
        {
            //Arrange
            var database = new Database();
            var bear = TestSetup.GetAnimals()[0];

            //Act
            database.AddAnimal(bear);

            //Assert
            Assert.IsTrue(database.Animals.Contains(bear));
        }
    }
}
