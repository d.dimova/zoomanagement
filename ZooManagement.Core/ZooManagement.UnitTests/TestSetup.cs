using ZooManagement.Models;
using ZooManagement.Models.Abstracts;

namespace ZooManagement.UnitTests
{
    public static class TestSetup
    {
       public static Animal[] GetAnimals()
        {
            int keyStart = 1;

            return new Animal[]
            {
                new Bear(keyStart++, "Fuzzy"),
                new Bear(keyStart++, "Cuddles"),
                new Bear(keyStart++, "Chubby"),
                new Bear(keyStart++, "Baba"),
                new Giraffe(keyStart++, "Spot"),
                new Giraffe(keyStart++, "Bao"),
                new Giraffe(keyStart++, "Shorty"),
                new Giraffe(keyStart++, "Jeffrey"), 
                new Monkey(keyStart++, "Aldo"),
                new Monkey(keyStart++, "Bonzo"),
                new Monkey(keyStart++, "Lazlo"),
                new Monkey(keyStart++, "Raffles"),
            };
        }
    }
}
