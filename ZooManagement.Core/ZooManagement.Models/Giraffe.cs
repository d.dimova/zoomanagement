﻿using ZooManagement.Common;
using ZooManagement.Models.Abstracts;

namespace ZooManagement.Models
{
    public class Giraffe : Animal
    {
        private bool canMoveNeck;

        public Giraffe(int id, string name) : base(id, name)
        {
        }

        public override int Health
        {
            get => base.Health;
            private protected set
            {
                if (!this.canMoveNeck && value < this.Health)
                {
                    this.IsDead = true;
                }

                base.Health = value;
            }
        }

        public bool CanMoveNeck
        {
            get
            {
                if (this.Health < 60)
                {
                    this.canMoveNeck = false;
                }
                else
                {
                    this.canMoveNeck = true;
                }

                return this.canMoveNeck;
            }
        }

        public override string ToString()
        {
            return string.Format(Strings.GiraffeInfo, this.Name, this.ID, this.Health, this.CanMoveNeck, this.IsDead ? "dead" : "alive");
        }
    }
}
