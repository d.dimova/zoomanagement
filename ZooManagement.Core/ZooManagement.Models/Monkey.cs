﻿using ZooManagement.Common;
using ZooManagement.Models.Abstracts;

namespace ZooManagement.Models
{
    public class Monkey : Animal
    {
        public Monkey(int id, string name) : base(id, name)
        {
        }

        public override int Health
        {
            get => base.Health;
            private protected set
            {
                if (value < 40)
                {
                    this.IsDead = true;
                }

                base.Health = value;
            }
        }

        public override string ToString()
        {
            return string.Format(Strings.MonkeyInfo, this.Name, this.ID, this.Health, this.IsDead ? "dead" : "alive");
        }
    }
}
