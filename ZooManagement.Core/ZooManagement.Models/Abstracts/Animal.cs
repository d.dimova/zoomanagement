﻿using System;
using ZooManagement.Common;

namespace ZooManagement.Models.Abstracts
{
    public abstract class Animal
    {
        private string name;
        private int health;

        public Animal(int id, string name)
        {
            this.ID = id;
            this.Name = name;
            this.Health = 100;
            this.IsDead = false;
        }

        public int ID { get; }

        public string Name
        {
            get => this.name;
            private set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(Strings.NameCannotBeNull);
                }

                this.name = value;
            }
        }

        public virtual int Health 
        { 
            get => this.health; 
            private protected set
            {
                if (value > 100)
                {
                    this.health = 100;
                    return;
                }
                else if (value < 0)
                {
                    this.health = 0;
                    return;
                }

                this.health = value;
            }
        }

        public bool IsDead { get; private protected set; }

        public string Eat(int amount)
        {
            this.Health += amount;

            return string.Format(Strings.AnimalIsFeeded, this.Name, this.Health);
        }

        public string GetHungry(int amount)
        {
            this.Health -= amount;

            var message = string.Format(Strings.AnimalIsHungry, this.Name, this.Health);

            if (this.IsDead)
            {
                message += Environment.NewLine + string.Format(Strings.AnimalDied, this.Name, Console.ForegroundColor = ConsoleColor.Gray);
            }

            return message;
        }

        public override string ToString()
        {
            return string.Format(Strings.AnimalInfo, this.Name, this.ID, this.Health, this.IsDead ? "dead" : "alive");
        }
    }
}
