﻿using Microsoft.Extensions.DependencyInjection;
using System;
using ZooManagement.Contracts.Data;
using ZooManagement.Core.Contracts;
using ZooManagement.Data;
using ZooManagement.Services;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Core
{
    public class StartUp
    {
        private static IServiceProvider serviceProvider;

        static void Main()
        {
            RegisterServices();
            IServiceScope scope = serviceProvider.CreateScope();

            scope.ServiceProvider.GetRequiredService<IEngine>().Run();
            DisposeServices();
        }

        private static void RegisterServices()
        {
            var services = new ServiceCollection();
            services.AddSingleton<IEngine, Engine>();
            services.AddSingleton<ICommandManager, CommandManager>();
            services.AddSingleton<IFactory, Factory>();
            services.AddSingleton<IDatabase, Database>();
            serviceProvider = services.BuildServiceProvider(true);
        }

        private static void DisposeServices()
        {
            if (serviceProvider == null)
            {
                return;
            }
            if (serviceProvider is IDisposable)
            {
                ((IDisposable)serviceProvider).Dispose();
            }
        }
    }
}
