﻿using System;
using System.Text;
using ZooManagement.Common;
using ZooManagement.Core.Contracts;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Core
{
    public class Engine : IEngine
    {
        private readonly ICommandManager commandManager;

        public Engine(ICommandManager commandManager)
        {
            this.commandManager = commandManager;
        }

        public void Run()
        {
            Console.WriteLine("To view the menu, type \"Menu\".");

            while (true)
            {
                string input = this.Read();

                if (input == "end")
                    break;

                string result = this.Process(input);

                this.Print(result);
            }
        }

        private string Read()
        {
            string input = Console.ReadLine();

            return input;
        }

        private string Process(string commandLine)
        {
            try
            {
                ICommand command = this.commandManager.ParseCommand(commandLine);
                string result = command.Execute();

                return result.Trim();
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                return string.Format(Strings.ErrorMessage, e.Message, Console.ForegroundColor = ConsoleColor.DarkRed);
            }
        }

        private void Print(string commandResult)
        {
            StringBuilder strB = new StringBuilder();

            strB.AppendLine(commandResult);

            Console.WriteLine(strB.ToString().Trim());
            Console.WriteLine(String.Format(Strings.Delimmiter, Console.ForegroundColor = ConsoleColor.Gray));
        }
    }
}
