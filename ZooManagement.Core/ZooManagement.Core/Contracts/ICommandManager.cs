﻿using ZooManagement.Services.Contracts;

namespace ZooManagement.Core.Contracts
{
    public interface ICommandManager
    {
        ICommand ParseCommand(string commandLine);
    }
}
