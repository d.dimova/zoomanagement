﻿namespace ZooManagement.Core.Contracts
{
    public interface IEngine
    {
        void Run();
    }
}
