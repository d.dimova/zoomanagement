﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Core.Contracts;
using ZooManagement.Services.Commands;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Core
{
    public class CommandManager : ICommandManager
    {
        private readonly IDatabase database;
        private readonly IFactory factory;

        public CommandManager(IDatabase database, IFactory factory)
        {
            this.database = database;
            this.factory = factory;
        }

        public ICommand ParseCommand(string commandLine)
        {
            List<string> lineParameters = commandLine.Split(" ", StringSplitOptions.RemoveEmptyEntries).ToList();

            string commandName = lineParameters[0].ToLower();
            List<string> commandParameters = lineParameters.Skip(1).ToList();

            return commandName switch
            {
                "addanimal" => new AddAnimalCommand(commandParameters, this.database, this.factory),
                "feed" => new FeedAnimalsCommand(commandParameters, this.database, this.factory),
                "gethungry" => new GetHungryCommand(commandParameters, this.database, this.factory),
                "getminimalhealth" => new GetMinimalHealthCommand(commandParameters, this.database, this.factory),
                "menu" => new MenuCommand(commandParameters, this.database, this.factory),
                "viewanimals" => new ViewAllAnimalsCommand(commandParameters, this.database, this.factory), 
                "viewdeadanimals" => new ViewAllDeadAnimalsCommand(commandParameters, this.database, this.factory),

                _ => throw new ArgumentException(String.Format(Strings.CommandNotVallid))
            };
        }
    }
}
