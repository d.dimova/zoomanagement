﻿using System;

namespace ZooManagement.Common
{
    public static class Strings
    {
        //Engine
        public const string ErrorMessage = "ERROR: {0}";
        public const string Delimmiter = "##########";

        //Models info
        public const string AnimalDied = "{0} is not strong enough. Unfortunately the animal is dead.";
        public const string AnimalIsFeeded = "{0} has been feeded. Now the animal has {1} health points.";
        public const string AnimalIsHungry = "{0} is getting hungry. Now the animal has {1} health points.";
        public const string AnimalIsAdded = "{0} is added to the zoo.";
        public const string AllAnymalsAreDead = "Unfortunately all {0}s are dead.";
        public const string NoDeadAnimals = "There are no dead animals.";

        public const string AnimalInfo = "Animal: {0}----\r\nID: {1}, Health: {2}, Death condition: {3}.";
        public const string BearInfo = "Bear: {0} ----\r\nid: {1}, Health: {2}, Death condition: {3}.";
        public const string GiraffeInfo = "Giraffe: {0} ----\r\nID: {1}, Health: {2}, Can move neck: {3}, Death condition: {4}.";
        public const string MonkeyInfo = "Monkey: {0} ----\r\nID: {1}, Health: {2}, Death condition: {3}.";

        //Validator message
        public const string NameCannotBeNull = "Name cannot be null or empty.";
        public const string CommandNotVallid = "The command you wanted to execute is not valid.";
        public const string InvalidAnimalType = "The animal you wanted to add is not existing.";

        //Menu
        public const string MenuCommandPath = @"..\..\..\..\menuCommand.txt";
        public const string FileNotFound = "File with path {0} not found.";
    }
}
