﻿using System.Collections.Generic;
using ZooManagement.Models;
using ZooManagement.Models.Abstracts;

namespace ZooManagement.Data
{
    public static class DataExtensions
    {
        public static void Seed(this List<Animal> animals)
        {
            int keyStart = 1;
            animals.AddRange(CreateBears(ref keyStart));
            animals.AddRange(CreateGiraffes(ref keyStart));
            animals.AddRange(CreateMonkeys(ref keyStart));
        }

        public static Bear[] CreateBears(ref int keyStart)
        {
            return new Bear[]
            {
                new Bear(keyStart++, "Fuzzy"),
                new Bear(keyStart++, "Cuddles"),
                new Bear(keyStart++, "Chubby"),
                new Bear(keyStart++, "Baba"),
                new Bear(keyStart++, "Honey"),
                new Bear(keyStart++, "Buttons"),
                new Bear(keyStart++, "Tuffy"),
                new Bear(keyStart++, "Pebbles"),
                new Bear(keyStart++, "Huggie"),
                new Bear(keyStart++, "Snuggles"),
            };
        }

        public static Giraffe[] CreateGiraffes(ref int keyStart)
        {
            return new Giraffe[]
            {
                new Giraffe(keyStart++, "Spot"),
                new Giraffe(keyStart++, "Bao"),
                new Giraffe(keyStart++, "Shorty"),
                new Giraffe(keyStart++, "Jeffrey"),
                new Giraffe(keyStart++, "Numbolelo"),
                new Giraffe(keyStart++, "Finn"),
                new Giraffe(keyStart++, "Tiny"),
                new Giraffe(keyStart++, "Stretch"),
                new Giraffe(keyStart++, "Lawrence Longneck"),
                new Giraffe(keyStart++, "Sky"),
            };
        }

        public static Monkey[] CreateMonkeys(ref int keyStart)
        {
            return new Monkey[]
            {
                new Monkey(keyStart++, "Aldo"),
                new Monkey(keyStart++, "Bonzo"),
                new Monkey(keyStart++, "Lazlo"),
                new Monkey(keyStart++, "Raffles"),
                new Monkey(keyStart++, "Dunston"),
                new Monkey(keyStart++, "King Kong"),
                new Monkey(keyStart++, "Rafiki"),
                new Monkey(keyStart++, "Louie"),
                new Monkey(keyStart++, "Jack"),
                new Monkey(keyStart++, "Congo"),
            };
        }
    }
}
