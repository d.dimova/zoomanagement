﻿using System.Collections.Generic;
using ZooManagement.Models;
using ZooManagement.Models.Abstracts;

namespace ZooManagement.Contracts.Data
{
    public interface IDatabase
    {
        List<Animal> Animals { get; }

        void AddAnimal(Animal animal);
    }
}
