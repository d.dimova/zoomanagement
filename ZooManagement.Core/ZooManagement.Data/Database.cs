﻿using System.Collections.Generic;
using ZooManagement.Contracts.Data;
using ZooManagement.Models.Abstracts;

namespace ZooManagement.Data
{
    public class Database : IDatabase
    {
        private readonly List<Animal> animals;

        public Database()
        {
            this.animals = new List<Animal>();
            this.animals.Seed();
        }

        public List<Animal> Animals { get => this.animals; }

        public void AddAnimal(Animal animal)
        {
            this.Animals.Add(animal);
        }
    }
}
