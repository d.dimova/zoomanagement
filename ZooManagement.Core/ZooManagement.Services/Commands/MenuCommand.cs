﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Services.Commands
{
    public class MenuCommand : Command
    {
        public MenuCommand(IList<string> commandParameters
           , IDatabase database
           , IFactory factory) : base(commandParameters, database, factory)
        {
        }
        public override string Execute()
        {
            try
            {
                StreamReader reader = new StreamReader(Strings.MenuCommandPath);

                using (reader)
                {
                    return reader.ReadToEnd();
                }
            }
            catch (FileNotFoundException)
            {
                throw new FileNotFoundException(string.Format(Strings.FileNotFound, Strings.MenuCommandPath));
            }
        }
    }
}
