﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Services.Commands
{
    public class FeedAnimalsCommand : Command
    {
        public FeedAnimalsCommand(IList<string> commandParameters
            , IDatabase database
            , IFactory factory) : base(commandParameters, database, factory)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 0)
            {
                throw new ArgumentException(Strings.CommandNotVallid);
            }

            var strB = new StringBuilder();
            var animalsCount = this.Database.Animals.Count();
            var rand = new Random();

            var animalsToFeed = this.Database.Animals.OrderByDescending(animal => animal.Health).Take((int)Math.Floor(animalsCount * 0.9)).ToList();

            foreach (var animal in animalsToFeed)
            {
                var health = rand.Next(10, 26);

                strB.Append(animal.Eat(health) + Environment.NewLine + Strings.Delimmiter + Environment.NewLine);
            }

            return strB.Remove(strB.Length - 12, 10).ToString();
        }
    }
}
