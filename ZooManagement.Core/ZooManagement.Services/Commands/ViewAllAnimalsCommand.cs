﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Services.Commands
{
    public class ViewAllAnimalsCommand : Command
    {
        public ViewAllAnimalsCommand(IList<string> commandParameters
            , IDatabase database
            , IFactory factory) : base(commandParameters, database, factory)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 0)
            {
                throw new ArgumentException(Strings.CommandNotVallid);
            }

            var strB = new StringBuilder();

            var aliveAnimals = this.Database.Animals.Where(animal => !animal.IsDead);

            if (aliveAnimals == null || aliveAnimals.Count() == 0)
            {
                return string.Format(Strings.AllAnymalsAreDead, "animal");
            }

            foreach (var animal in aliveAnimals)
            {
                strB.Append(animal.ToString() + Environment.NewLine + Strings.Delimmiter + Environment.NewLine);
            }

            return strB.Remove(strB.Length - 12, 10).ToString();
        }
    }
}
