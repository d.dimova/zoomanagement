﻿using System;
using System.Collections.Generic;
using System.Text;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Services.Commands
{
    public class GetHungryCommand : Command
    {
        public GetHungryCommand(IList<string> commandParameters
            , IDatabase database
            , IFactory factory) : base(commandParameters, database, factory)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 0)
            {
                throw new ArgumentException(Strings.CommandNotVallid);
            }

            var strB = new StringBuilder();
            var rand = new Random();

            var animalsToFeed = this.Database.Animals;

            foreach (var animal in animalsToFeed)
            {
                var health = rand.Next(15, 35);

                strB.Append(animal.GetHungry(health) + Environment.NewLine + Strings.Delimmiter + Environment.NewLine);
            }

            Console.ForegroundColor = ConsoleColor.Blue;
            return strB.Remove(strB.Length - 12, 10).ToString();
        }
    }
}
