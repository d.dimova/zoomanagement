﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Models.Abstracts;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Services.Commands
{
    public class AddAnimalCommand : Command
    {
        public AddAnimalCommand(IList<string> commandParameters
            , IDatabase database
            , IFactory factory) : base(commandParameters, database, factory) 
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 2)
            {
                throw new ArgumentException(Strings.CommandNotVallid);
            }

            var typeOfAnimal = this.CommandParameters[0].ToLower();
            var name = this.CommandParameters[1];

            var id = this.Database.Animals.Count() + 1;

            Animal animal = typeOfAnimal switch
            {
                "bear" => this.Factory.CreateBear(id, name),
                "giraffe" => this.Factory.CreateGiraffe(id, name),
                "monkey" => this.Factory.CreateMonkey(id, name),

                _ => throw new ArgumentException(Strings.InvalidAnimalType)
            };

            this.Database.AddAnimal(animal);

            return string.Format(Strings.AnimalIsAdded, animal.Name);
        }
    }
}
