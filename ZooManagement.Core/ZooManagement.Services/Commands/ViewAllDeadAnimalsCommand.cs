﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Services.Commands
{
    public class ViewAllDeadAnimalsCommand : Command
    {
        public ViewAllDeadAnimalsCommand(IList<string> commandParameters
            , IDatabase database
            , IFactory factory) : base(commandParameters, database, factory)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 0)
            {
                throw new ArgumentException(Strings.CommandNotVallid);
            }

            var strB = new StringBuilder();

            var deadAnimals = this.Database.Animals.Where(animal => animal.IsDead);

            if (deadAnimals == null || deadAnimals.Count() == 0)
            {
                return string.Format(Strings.NoDeadAnimals);
            }

            foreach (var animal in deadAnimals)
            {
                strB.Append(animal.ToString() + Environment.NewLine + Strings.Delimmiter + Environment.NewLine);
            }

            return strB.Remove(strB.Length - 12, 10).ToString();
        }
    }
}
