﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZooManagement.Common;
using ZooManagement.Contracts.Data;
using ZooManagement.Models;
using ZooManagement.Models.Abstracts;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Services.Commands
{
    public class GetMinimalHealthCommand : Command
    {
        public GetMinimalHealthCommand(IList<string> commandParameters
           , IDatabase database
           , IFactory factory) : base(commandParameters, database, factory)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 1)
            {
                throw new ArgumentException(Strings.CommandNotVallid);
            }

            var typeOfAnimal = this.CommandParameters[0].ToLower();
            var animals = this.Database.Animals.Where(animal => !animal.IsDead);

            Animal animal = null;

            switch (typeOfAnimal)
            {
                case "bear":
                    var bears = animals.Where(animal => animal is Bear);
                    animal = bears.FirstOrDefault(bear => bear.Health == bears.Min(b => b.Health));
                    break;
                case "giraffe":
                    var giraffes = animals.Where(animal => animal is Giraffe);
                    animal = giraffes.FirstOrDefault(giraffe => giraffe.Health == giraffes.Min(g => g.Health));
                    break;
                case "monkey":
                    var monkeys = animals.Where(animal => animal is Monkey);
                    animal = monkeys.FirstOrDefault(monkey => monkey.Health == monkeys.Min(m => m.Health));
                    break;
                default:
                    throw new ArgumentException(Strings.InvalidAnimalType);
            }

            if (animal == null)
            {
                return string.Format(Strings.AllAnymalsAreDead, typeOfAnimal);
            }
            return animal.ToString();
        }
    }
}
