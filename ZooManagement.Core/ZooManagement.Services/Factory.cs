﻿using ZooManagement.Models;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Services
{
    public class Factory : IFactory
    {
        public Bear CreateBear(int id, string name)
        {
            return new Bear(id, name);
        }

        public Giraffe CreateGiraffe(int id, string name)
        {
            return new Giraffe(id, name);
        }

        public Monkey CreateMonkey(int id, string name)
        {
            return new Monkey(id, name);
        }
    }
}
