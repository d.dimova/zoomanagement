﻿using System;
using System.Collections.Generic;
using System.Text;
using ZooManagement.Models;

namespace ZooManagement.Services.Contracts
{
    public interface IFactory
    {
        Bear CreateBear(int id, string name);

        Giraffe CreateGiraffe(int id, string name);

        Monkey CreateMonkey(int id, string name);
    }
}
