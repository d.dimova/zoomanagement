﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZooManagement.Services.Contracts
{
    public interface ICommand
    {
        string Execute();
    }
}
