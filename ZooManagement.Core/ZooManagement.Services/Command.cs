﻿using System.Collections.Generic;
using ZooManagement.Contracts.Data;
using ZooManagement.Services.Contracts;

namespace ZooManagement.Services
{
    public abstract class Command : ICommand
    {
        private readonly IFactory factory;
        private readonly IDatabase database;
        protected Command(IList<string> commandParameters, IDatabase database, IFactory factory)
        {
            this.CommandParameters = new List<string>(commandParameters);
            this.database = database;
            this.factory = factory;
        }

        protected IList<string> CommandParameters { get; }

        protected IDatabase Database { get => this.database; }

        protected IFactory Factory { get => this.factory; }

        public abstract string Execute();
    }
}
