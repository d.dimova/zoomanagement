# ZooManagement

Questions
1. What are the advantages and disadvantages of class inheritance?

Advantages:

- With inheritance we can reuse the base functionality in all derived classes without rewriting it.
- When testing we won't have to test those methods for every child class - they are already covered by the base class (except when the method is overridden and additional logic is added, some more tests need to be implemented).
- Due to its reusability, the code will be easier to maintain - if changes are needed they will be applied only in the base class.
- Creates abstraction - the child classes implement the interface/base class.

Disadvantages:

- Increases the coupling between the base class and the derived ones.
- If some mandatory changes are made to the base class (deleting, rewriting code), it will affect all derived classes and eventually create bugs whereever this logic was used.
- It is making the process time slower because some methods/properties are indirect.

2. Explain in your own words what you understand of the term "Generics" in .NET.

Generics means there is no specification to a data type. In C# they allow us to create generic classes, interfaces, state and behaviour of classes - in their definition a type parameter is used as a replacement for a particular type. The exact definition of the type that will be used within the generic type is specified when an instance is made. Also, I am using a generic class for example, we can define some restrictions to the definition of that class for what types it can be provided with by adding the where keyword.

3. Which .NET data structure would you prefer if you need to search for elements by key and
you’re optimizing for speed – Dictionary or SortedDictionary? Explain your choice.

In the case of searching elements by their key I would prefer the Dictionary because it uses hash table for it's keys. The hashing function always returns the same hash code for the same value that it's given. The index, to which the element is added is calculated by modular deviding the hash to the size of the array. If there was no collisions during the process of adding new items to the dictionary the complexity of searching will be O(1) and will be much faster than the SortedDictionary (which is implemented as a sorted binary tree and searching has a complexity of O(log(n))). In case of collisions the complexity will be 0(n) where n is the amount of iterations till finding the searched element.

4. Explain in your own words how memory is managed in .NET.

The memory is automatically managed by the Garbage collector in the CLR. It is used to release and allocate resources in the memory.
There are three generations - 0(for the data with short life like method variables), 1(for short-lived objects and an in-the-middle state for the long-lived ones) and 2(for long-lived objects).
- Generation 0 is the location for the newly created objects and the short-lived ones. If the object that needs to be stored is a large object it is redirected to the Large Objects Heap. When this generation is full, the garbage collection releases all unreachable objects and if any are left, they are allocated to generation 1.
- Generation 1 contains resources that were not released during the collection in generation 0. When this part is full the GC runs and does the same as explained earlier first in this generation and if any objects are left untouched, they are moved to generation 2 and then in generation 0.
- Generation 2 is for the long-lived resources of the application. If this part is full, a clean is run in all 3 generations with generation 2 as a starting point. The Large Objects Heap is beeing cleaned during releases in this generation. If after the collection is done there is still not enough memory for allocations a OutOfMemoryException is thrown. 